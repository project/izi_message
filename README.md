CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Izi Message is a very simple module that provides Drupal messages.
Elegant, responsive, flexible and light messaging module with library
[izitoast.marcelodolza.com](http://izitoast.marcelodolza.com)


RECOMMENDED MODULES
-------------------

 * No extra module is required.


INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8
   for further information.


CONFIGURATION
-------------

    1 Navigate to Administration > Configuration > Development >
      Izi message settings for configurations. Save Configurations.



REQUIREMENTS
------------

This module requires no modules outside of Drupal core,
but require next js library:

 * [iziToast](http://izitoast.marcelodolza.com)


MAINTAINERS
-----------

Current maintainers:

 * UsingSession (https://www.drupal.org/u/usingsession)
